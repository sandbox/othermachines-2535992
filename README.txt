# Bootstrap Material Design Theme for Drupal

A Bootstrap subtheme that brings the 'Material Design guidelines by Google' 
into Drupal (7.x). Uses Fez Vrasta's 'Material Design for Bootstrap' theme.

## Use

1. Download and extract the bootstrap_material/ subfolder into your Drupal 
   installation's themes folder (usually sites/themes/*).

2. Go to *admin/appearance* and enable the subtheme.

## Customize 

I use Node.js, Grunt and Bower to manage CSS and JavaScript assets. You can, 
too, but it requires a patch (see 'Links', below) to prevent the node_modules 
and bower_components directories from being scanned by Drupal. Otherwise it 
will crash hard.

1. Clone this project into your Drupal installation's themes directory.

2. Go to the project folder: 

    `cd bootstrap_material`

3. Install dependencies: 

    `npm install`

4. Initial set-up: 

    `grunt build`

5. To watch for changes: 

    `grunt`

Alternatively, since the CSS and JS is precompiled you can do some basic tweaks
by simply adding a custom CSS file to the *.info file.

## Links

Malformed theme .info files break menu_router generation
(Patch) https://www.drupal.org/files/issues/malformed_theme_info-619542-146.patch
(Issue: https://www.drupal.org/node/619542#comment-9771891)

Bootstrap Theme
https://www.drupal.org/project/bootstrap

Material Design guidelines by Google
http://www.google.com/design/spec/material-design/introduction.html

Material Design for Bootstrap
https://fezvrasta.github.io/bootstrap-material-design

